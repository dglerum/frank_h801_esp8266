<?php
//Script made by 'ThinkPad' for 'H801 WiFi dimmer' (based on ESP8266)
//See forumthread at http://domoticz.com/forum/viewtopic.php?f=38&t=7957
//Save as 'rgb.php' and call the script like rgb.php?hex=ff8d00

$host = 'udp://192.168.4.23';    
$port = 30977; 

//Strip out any invalid characters, only A-Z and numbers are allowed.
$color = preg_replace("/[^a-zA-Z0-9\s]/", "", $_GET["hex"]);

//Check if the colorcode sent has 6 values
if ( strlen($color) == 6 ) {
   $red = substr($color, 0, 2);
   $green = substr($color, 2, 2);
   $blue = substr($color, 4, 2);

   $fp = pfsockopen($host, $port);

//Color should be like 0xfbebRRGGBB000079979d00 (where RR, GG, BB are the hex values for the colors)
   $line="\xfb\xeb" . chr(hexdec($red)) . chr(hexdec($green)) .  chr(hexdec($blue)) . "\x00\x00\x79\x97\x9d\x00";
   fwrite($fp, $line);
   fclose($fp);

   echo  '<body bgcolor="#'. $color .'">';  
   echo 'Chosen color is: <a href="http://www.color-hex.com/color/' . $color . '"><b>' . $color . '</b></a>';
}
else {
   echo "Not a valid colorcode, has to be 6 characters!";
}


//Check if rgb.php?cmd=PWRON was received
if ($_GET["cmd"] == "PWRON"){
   $fp = pfsockopen($host, $port);
$line_on="\xfb\xeb\xff\x00\x3c\x00\x00\x79\x97\x9d\x00"; //Power on
$line_normalmode="\xfb\xec\x00\x1e\x00\x00\x00\x79\x97\x9d\x00"; //Set controller into mode to accept colorcodes

$x = 1;

//Send 'power on' command 6 times
while($x <= 6) {
   fwrite($fp, $line_on);
   fclose($fp);
   $x++;
} 

//Send 'back to colorcode-mode' command 3 times
while($x <= 3) {
   fwrite($fp, $line_normalmode);
   fclose($fp);
   $x++;
} 

}

//Check if rgb.php?cmd=PWROFF was received
if ($_GET["cmd"] == "PWROFF"){
   $fp = pfsockopen($host, $port);
$line_off="\xfb\xeb\x00\x00\x00\x00\x00\x79\x97\x9d\x00"; //Power off

//Send 'power off' command 6 times
while($x <= 6) {
   fwrite($fp, $line_off);
   fclose($fp);
   $x++;
} 

}

?>